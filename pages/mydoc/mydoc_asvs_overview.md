---
title: Application Security Verification Standard (ASVS)
tags: [asvs, owasp]
summary: "What is ASVS and how does it apply to Psono."
sidebar: mydoc_sidebar
permalink: mydoc_asvs_overview.html
folder: mydoc
---

## What is ASVS in general?

The OWASP Application Security Verification Standard (ASVS) Project provides a basis for testing web application technical security controls and also provides developers with a list of requirements for secure development.

The primary aim of the OWASP Application Security Verification Standard (ASVS) Project is to normalize the range in the coverage and level of rigor available in the market when it comes to performing Web application security verification using a commercially-workable open standard. The standard provides a basis for testing application technical security controls, as well as any technical security controls in the environment, that are relied on to protect against vulnerabilities such as Cross-Site Scripting (XSS) and SQL injection. This standard can be used to establish a level of confidence in the security of Web applications. The requirements were developed with the following objectives in mind:

* Use as a metric - Provide application developers and application owners with a yardstick with which to assess the degree of trust that can be placed in their Web applications,
* Use as guidance - Provide guidance to security control developers as to what to build into security controls in order to satisfy application security requirements, and
* Use during procurement - Provide a basis for specifying application security verification requirements in contracts.

## How is it applicable to Psono?

Psono as a password manager has to live up to the highest standards of internet security. We want to provide here a 
self audit of Psono (the application) and Psono SaaS (psono.pw).

Our goals are:

* to improve the general security awareness for contributers and developers 
* create a brief overview / base for auditors to evaluate security measures
* demonstrate to interested parties how Psono is designed

All questions have been answered March 6th, 2018. The version of ASVS that is used for this self audit is ASVS 3.0.1.

## AVAST License (applicable to the whole ASVS section)

Copyright © 2008 – 2016 The OWASP Foundation.

This document is released under the Creative Commons Attribution ShareAlike 3.0 license. For any reuse or distribution, you must make clear to others the license terms of this work.

## Authors of AVAST

### Version 3.0, 2015

|Project Leads	| Lead Authors	| Contributors and Reviewers |
| --- | --- | --- |
| Andrew van der Stock<br>Daniel Cuthbert | Jim Manico | Abhinav Sejpal<br>Ari Kesäniemi<br>Boy Baukema<br>Colin Watson<br>Cristinel Dumitru<br>David Ryan<br>François-Eric Guyomarc’h<br>Gary Robinson<br>Glenn Ten Cate<br>James Holland<br>Martin Knobloch<br>Raoul Endres<br>Ravishankar S<br>Riccardo Ten Cate<br>Roberto Martelloni<br>Ryan Dewhurst<br>Stephen de Vries<br>Steven van der Baan |


### Version 2.0, 2014

|Project Leads	| Lead Authors	| Contributors and Reviewers |
| --- | --- | --- |
| Daniel Cuthbert<br>Sahba Kazerooni | Andrew van der Stock<br>Krishna Raja | Antonio Fontes<br>Archangel Cuison<br>Ari Kesäniemi<br>Boy Baukema<br>Colin Watson<br>Dr Emin Tatli<br>Etienne Stalmans<br>Evan Gaustad<br>Jeff Sergeant<br>Jerome Athias<br>Jim Manico<br>Mait Peekma<br>Pekka Sillanpää<br>Safuat Hamdy<br>Scott Luc<br>Sebastien Deleersnyder |


### Version 1.0, 2009


|Project Leads	| Lead Authors	| Contributors and Reviewers |
| --- | --- | --- |
| Mike Boberski<br>Jeff Williams<br>Dave Wichers | Jim Manico | Andrew van der Stock<br>Barry Boyd<br>Bedirhan Urgun<br>Colin Watson<br>Dan Cornell<br>Dave Hausladen<br>Dave van Stein<br>Dr. Sarbari Gupta<br>Dr. Thomas Braun<br>Eoin Keary<br>Gaurang Shah<br>George Lawless<br>Jeff LoSapio<br>Jeremiah Grossman<br>John Martin<br>John Steven<br>Ken Huang<br>Ketan Dilipkumar Vyas<br>Liz FongShouvik Bardhan<br>Mandeep Khera <br>Matt Presson<br>Nam Nguyen<br>Paul Douthit<br>Pierre Parrend<br>Richard Campbell<br>Scott Matsumoto<br>Stan Wisseman<br>Stephen de Vries<br>Steve Coyle<br>Terrie Diaz<br>Theodore Winograd |



## Source

[https://www.owasp.org/index.php/Category:OWASP_Application_Security_Verification_Standard_Project](https://www.owasp.org/index.php/Category:OWASP_Application_Security_Verification_Standard_Project)
[https://www.owasp.org/images/3/33/OWASP_Application_Security_Verification_Standard_3.0.1.pdf](https://www.owasp.org/images/3/33/OWASP_Application_Security_Verification_Standard_3.0.1.pdf)

