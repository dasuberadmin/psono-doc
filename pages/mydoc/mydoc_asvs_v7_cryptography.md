---
title: V7 Cryptography
tags: [asvs, owasp]
summary: "Cryptography at rest verification requirements"
sidebar: mydoc_sidebar
permalink: mydoc_asvs_v7_cryptography.html
folder: mydoc
---

## ASVS Verification Requirement

| ID                          | Detailed Verification Requirement                                                                                                                                                                                                                                                                                                                              | Level 1                 | Level 2    | Level 3     | Since     | 
|-----------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------|------------|-------------|-----------| 
| 7.2                         | Verify that all cryptographic modules fail securely, and errors are handled in a way that does not enable oracle padding.                                                                                                                                                                                                                                      | x                       | x          | x           | 1.0       | 
| 7.6                         | Verify that all random numbers, random file names, random GUIDs, and random strings are generated using the cryptographic module’s approved random number generator when these random values are intended to be not guessable by an attacker.                                                                                                                  |                         | x          | x           | 1.0       | 
| 7.7                         | Verify that cryptographic algorithms used by the application have been validated against FIPS 140-2 or an equivalent standard.                                                                                                                                                                                                                                 | x                       | x          | x           | 1.0       | 
| 7.8                         | Verify that cryptographic modules operate in their approved mode according to their published security policies.                                                                                                                                                                                                                                               |                         |            | x           | 1.0       | 
| 7.9                         | Verify that there is an explicit policy for how cryptographic keys are managed (e.g., generated, distributed, revoked, and expired). Verify that this key lifecycle is properly enforced.                                                                                                                                                                      |                         | x          | x           | 1.0       | 
| 7.11                        | Verify that all consumers of cryptographic services do not have direct access to key material. Isolate cryptographic processes, including master secrets and consider the use of a virtualized or physical hardware key vault (HSM).                                                                                                                           |                         |            | x           | 3.0.1     | 
| 7.12                        | Personally Identifiable Information should be stored encrypted at rest and ensure that communication goes via protected channels.                                                                                                                                                                                                                              |                         | x          | x           | 3.0       | 
| 7.13                        | Verify that sensitive passwords or key material maintained in memory is overwritten with zeros as soon as it no longer required, to mitigate memory dumping attacks.                                                                                                                                                                                           |                         | x          | x           | 3.0.1     | 
| 7.14                        | Verify that all keys and passwords are replaceable, and are generated or replaced at installation time.                                                                                                                                                                                                                                                        |                         | x          | x           | 3.0       | 
| 7.15                        | Verify that random numbers are created with proper entropy even when the application is under heavy load, or that the application degrades gracefully in such circumstances.                                                                                                                                                                                   |                         |            | x           | 3.0       | 


### 7.2 (need help to check)

I need some help to check this.

### 7.6

There are three components that generate random numbers that should not be guessable by an attacker:

* Python "NaCl": We are using os.urandom, which according to [https://docs.python.org/2/library/os.html](https://docs.python.org/2/library/os.html#os.urandom) is `suitable for cryptographic use`.

* Python "uuid.uuid4()": Users os.urandom too. The corresponding code looks like this:

{% highlight python %}
def uuid4():
	"""Generate a random UUID."""
	return UUID(bytes=os.urandom(16), version=4)
{% endhighlight %}
	    
* Javascript: Checks if the browser can provide a cryptographically secure random number generator and if not fails. The corresponding code looks like this:

{% highlight javascript %}
 /**
  * Random byte generator from nacl_factory.js
  * https://github.com/tonyg/js-nacl
  *
  * @param {int} count The amount of random bytes to return
  *
  * @returns {Uint8Array} Random byte array
  */
 function randomBytes(count) {
     let bs;
     if (typeof module !== 'undefined' && module.exports) {
         // add node.js implementations
         const crypto = require('crypto');
         return crypto.randomBytes(count);
     } else if (window && window.crypto && window.crypto.getRandomValues) {
         // add in-browser implementation
         bs = new Uint8Array(count);
         window.crypto.getRandomValues(bs);
         return bs;
     } else if (window && window.msCrypto && window.msCrypto.getRandomValues) {
         // add in-browser implementation
         bs = new Uint8Array(count);
         window.msCrypto.getRandomValues(bs);
         return bs;
     } else {
         throw new Error('No cryptographic random number generator');
     }
 }
{% endhighlight %}

### 7.7

NACL was released 2008, while the latest publication update on "FIPS 140-2" was 2002.

The basic functions (from wikipedia):

Public-key cryptography:

- Authenticated encryption using Curve25519, Salsa20, and Poly1305.
- Signatures using Ed25519.
- Key agreement using Curve25519.

Secret-key cryptography:
- Authenticated encryption using Salsa20 and Poly1305.
- Encryption using Salsa20 or AES.
- Authentication using HMAC-SHA-512-256.
- One-time authentication using Poly1305.

Low-level functions:
- Hashing using SHA-512 or SHA-256.
- String comparison.

None of them have been broken and are widely spread and used.

### 7.8 (need help to check)

I don't know how to check this.

### 7.9

Cryptographic keys are generated directly on the target machine.

### 7.11

No consumer of client (consumers of cryptographic services) has access to any server key

### 7.12

Personal identifying information collected by psono is very limited. The few examples (e.g. the email address)
that is collected, is stored encrypted in the database. The information itself in transit is protected with multiple layers
of encryption, namely TLS and Psonos transport encryption layer.

### 7.13 (need help to check)

I have checked but couldn't find any information whether the cryptographic libraries are doing this or not.

### 7.14 (violation)

Even so all passwords are replaceable, the keys used by psono aren't. More details can be found here:
https://gitlab.com/psono/psono-server/issues/2

### 7.15

The application will degrace gracefully if the server's entropy is running low. To improve performance haveged has been 
deployed on all machines.
