---
title: V4 Access control
tags: [asvs, owasp]
summary: "Access control verification requirements"
sidebar: mydoc_sidebar
permalink: mydoc_asvs_v4_access_control.html
folder: mydoc
---

## ASVS Verification Requirement

| ID                          | Detailed Verification Requirement                                                                                                                                                                                                                                                                                                                              | Level 1                 | Level 2    | Level 3     | Since     | 
|-----------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------|------------|-------------|-----------| 
| 4.1                         | Verify that the principle of least privilege exists - users should only be able to access functions, data files, URLs, controllers, services, and other resources, for which they possess specific authorization. This implies protection against spoofing and elevation of privilege.                                                                         | x                       | x          | x           | 1.0       | 
| 4.4                         | Verify that access to sensitive records is protected, such that only authorized objects or data is accessible to each user (for example, protect against users tampering with a parameter to see or alter another user's account).                                                                                                                             | x                       | x          | x           | 1.0       | 
| 4.5                         | Verify that directory browsing is disabled unless deliberately desired. Additionally, applications should not allow discovery or disclosure of file or directory metadata, such as Thumbs.db, .DS_Store, .git or .svn folders.                                                                                                                                 | x                       | x          | x           | 1.0       | 
| 4.8                         | Verify that access controls fail securely.                                                                                                                                                                                                                                                                                                                     | x                       | x          | x           | 1.0       | 
| 4.9                         | Verify that the same access control rules implied by the presentation layer are enforced on the server side.                                                                                                                                                                                                                                                   | x                       | x          | x           | 1.0       | 
| 4.10                        | Verify that all user and data attributes and policy information used by access controls cannot be manipulated by end users unless specifically authorized.                                                                                                                                                                                                     |                         | x          | x           | 1.0       | 
| 4.11                        | Verify that there is a centralized mechanism (including libraries that call external authorization services) for protecting access to each type of protected resource.                                                                                                                                                                                         |                         |            | x           | 1.0       | 
| 4.12                        | Verify that all access control decisions can be logged and all failed decisions are logged.                                                                                                                                                                                                                                                                    |                         | x          | x           | 2.0       | 
| 4.13                        | Verify that the application or framework uses strong random anti-CSRF tokens or has another transaction protection mechanism.                                                                                                                                                                                                                                  | x                       | x          | x           | 2.0       | 
| 4.14                        | Verify the system can protect against aggregate or continuous access of secured functions, resources, or data. For example, consider the use of a resource governor to limit the number of edits per hour or to prevent the entire database from being scraped by an individual user.                                                                          |                         | x          | x           | 2.0       | 
| 4.15                        | Verify the application has additional authorization (such as step up or adaptive authentication) for lower value systems, and / or segregation of duties for high value applications to enforce anti-fraud controls as per the risk of application and past fraud.                                                                                             |                         | x          | x           | 3.0       | 
| 4.16                        | Verify that the application correctly enforces context-sensitive authorisation so as to not allow unauthorised manipulation by means of parameter tampering.                                                                                                                                                                                                   | x                       | x          | x           | 3.0       | 



### V4.1

Currently 2 Users groups exist. Normal users and so called "superusers". Normal users cannot access any resources behind
the `/admin/...` endpoint. All other ressources allow only access to entities with the adequat access level. Even if they
would gain access to entities (e.g. passwords), they would not be able to decrypt them.

### V4.4

The `parameter` that identifies a user is his Authorization Token that is transmitted in the Header of every request.
The likelihood of guessing another users token is is with a random token a length of 512 bits extreme small.
If the attacker would gain by whatever cause access to a token (e.g. spying on network traffic) he would still need various
other information that is never transmitted unencrypted (the users secret or private key, or the session secret).

### V4.5

Directory browsing is disabled in all offered configurations, yet is the responsibility of the administrator. Psono.pw has
been checked and does not allow directory browsing.

### V4.8

Access control fails securely. Access to `/admin/` resources require the superuser flag. Psono.pw uses IP whitelisting to restrict
access to the `/admin` resource path.

### V4.9

The server enforces all access controls.

### 4.10

The only `user and data attributes and policy information used by access controls` are the Token. The token is securely
stored as sha512 hash in the database. Due to the limited time a token is valid (by default max 30 days), the sha512
hash impose a strong enough protection against 'lost databases' or rogue database administrators, minimizing the risk
for impersonation attacks.

### 4.11

The authorization mechanism has been implemented only in one centralized place in the application and is reused across
the whole application. The same goes for all two-factor-authentication and LDAP modules.

### 4.12

All successful and failed calls to the API are logged as part of the Audit Logging in the Enterprise Edition


### 4.13

The risk of CSRF is due to the use of Headers instead of Cookies extremely limited, as they are not transmitted
automatically by the browser with every request. If someone would be able to somehow trick the browser into providing
such a header, he would still need to be able to generate the so called "Authorization Verification Header", that is
unique for every device, encrypted by the session secret and contains a time stamp that is only limited for a short
time (usually a couple of seconds). 


### 4.14

A ressource specific rate limiting is in place to protect the server's data from being scraped


### 4.15

The application follows a modular approach. Every subsystem is separated in docker images and can be hosted isolated.
The higher level components like the psono server and the database should be protected by firewalls.


### 4.16

All parameters are validated in the backend and proper access control is enforced.


