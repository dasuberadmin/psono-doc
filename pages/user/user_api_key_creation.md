---
title: Creation of the API key
tags: [API Key]
summary: "Instructions how to create the API key."
sidebar: user_sidebar
permalink: user_api_key_creation.html
folder: user
---

## Preamble

In order to create an API key you need to have an account and be logged in with any browser client.

## Guide

1)  Go to to "other":

![Step 1 Go to Other](images/user/api_key_creation/step1-go-to-other.jpg)

2)  Click "Create new API key":

![Step 2 Click create new API key](images/user/api_key_creation/step2-create-new-api-key.jpg)

3)  Configure your API key:

Next to the title you can the secrets that you want to make accessible with this api key, specify if you want to
"Allow insecure usage" (e.g. usage with remote decryption) or "secret restriction" (e.g. usage with sessions)

![Step 3 Configure your API key](images/user/api_key_creation/step3-specify-title-settings-and-add-secrets.jpg)

4)  Edit the created API key:

After clicking save click on the wrench symbol to edit the entry and display the actual secret key properties.

![Step 4 Edit the created API key](images/user/api_key_creation/step4-edit-the-created-api-key.jpg)

5)  Copy details:

Copy the shown details.

![Step 2 Copy details](images/user/api_key_creation/step5-copy-details.jpg)

6)  (optional) Copy secret ids:

If you are using the API key without session you need the secret ids you want to retrieve from the server

![Step 6 Copy secret ids](images/user/api_key_creation/step6-copy-secret-id.jpg)


{% include links.html %}
