---
title: File Repository Overview
tags: [File Repository]
summary: "Overview of how to use file repositories."
sidebar: user_sidebar
permalink: user_file_repository_overview.html
folder: user
---

## Preamble

File repositories are external storage provider (e.g. Google Cloud Storage or AWS S3) that can be used to upload, download and share encrypted data.
The file is split up in chunks, encrypted in the browser, and then uploaded to the repository. The data is uploaded directly to the repository
without passing through the psono server. The file repository provider has no way to gain access to the decrypted files.
File repositories can be shared with other users, allowing them to use the configured repository to upload data themself.

## Supported Provider

Currently Psono supports:

| Provider | Mechanism | Config |
| Google Cloud Storage | Signed URLs | [Config](/user_file_repository_setup_gcs.html) |
| AWS S3 | Signed URLs | [Config](/user_file_repository_setup_aws.html) |

## Permissions

File repository configuration can be shared with other users to allow them to upload something to the providers themself,
without the hassle to configure the repository on their own too.
The possibility to download a file is controlled similar to the capabilities to access secrets by the share permissions.

The permissions in detail:

| Shared | Read | Write | Grant | Resulting Rights |
| - | --------- | ----------------- | ----------------- |
| No | - | - | - | Can only download a file (if the share permissions allow him that) |
| Yes | No | No | No | Can upload files to this file repository, yet no administrative capabilities like accessing the config or sharing the repository with others |
| Yes | Yes | No | No | Can read the configuration of the external storage provider |
| Yes | No | Yes | No | Can update the configuration of the external storage provider |
| Yes | No | No | Yes | Can change the rights of himself and other users and share the repository with other users |

{% include note.html content="Usually the sole creation of a permission without read, write and grant is what you want, allowing the other user to use the file repository to upload data." %}

{% include links.html %}
