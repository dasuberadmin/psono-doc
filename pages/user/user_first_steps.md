---
title: First Steps
tags: [userguide]
summary: "Your first steps with Psono covering the basic steps to create an account and protect it."
sidebar: user_sidebar
permalink: user_first_steps.html
folder: user
---


1)  Start Registration

Quickest way to start is to visit our Web Client’s Registration Form. Follow the instructions. At the end an e-mail is sent to you to activate your account. In this step all the initial secrets are generated. Those initial secrets will later encrypt all your data before it leaves your browser and is stored on our server. The same goes for the initial secrets themself, the only difference is, that they are encrypted with your password before they leave your browser.

![Step 1 Registration](images/user/first_steps/0_registration.jpg)

2)  First Login

After registration you can now login to our Web Client. The Web Client will download the encrypted secrets in the background and decrypt them with your password.

![Step 2 Login](images/user/first_steps/1_login.jpg)

3)  First Password

Now its time to create our first password. Right Click into your datastore and select “New Entry”. You can create the same way folders and move items around per drag’n drop. The password datastore is the central place where all your notes, passwords and other secrets live and is encrypted with the initial secrets we created during registration.

![Step 3 Login](images/user/first_steps/2_first_password.jpg)

4)  Import Passwords

You were already using another password manager? Psono offers a migration feature to easily import other password manager’s export files. If your password manager is not listed, then feel free to open a Feature Request with a dummy export, to speed things up.

![Step 4 Import Passwords](images/user/first_steps/3_import_passwords.jpg)

5)  Generate Recovery Code

Humans tend to forget passwords. The only option to recover an account when you lose your password is with a recovery code. We advise everybody to create one and store it in a secure place. Every account has a maximum of one recovery code. Creating a new one will always overwrite any existing old one.

![Step 5 Generate Recovery Code](images/user/first_steps/4_recovery_code.jpg)

6)  Setup Multifactor Authentication

The best way to protect your account in case of a password breach is a second factor. Psono supports currently Google Auhenticator (and all compatible apps e.g. Authy) and YubiKey OTP which can be used in conjunction for three factor authentication.

![Step 6 Setup Multifactor Authentication](images/user/first_steps/5_multifactor.jpg)

7)  Download Extension

The usual way now to access your passwords is to open psono.pw in your browser. For a more convinient access and more features, download and install one of our extensions (available for Chrome and Firefox). The extension also offer some extended functionality like auto fill of password forms, qicksearch (type “pp” followed by a space into your addressbar), advanced security and much more.

![Step 7 Download Extension](images/user/first_steps/6_download_chrome.jpg)

{% include links.html %}